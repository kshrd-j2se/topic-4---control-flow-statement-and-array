package org.kshrd.loops;

public class Continue {

    public static void main(String[] args) {


        int numbers[] = {10, 20, 30, 40};


        // Continue
        for (int x : numbers) {
            if (x == 30) {
                continue;
            }
            System.out.println("x is " + x);
        }


        // continue label
        label:
        for (int i = 0; i < 3; i++) {
            for (int x : numbers) {
                if (x == 30) {
                    continue label;
                }
                System.out.println("x is " + x);
            }
            System.out.println("i = " + i);
        }


    }
}
