package org.kshrd.loops;

public class Break {


    public static void main(String[] args) {

        int numbers[] = {10, 20, 30, 40};


        // break
        for (int x : numbers) {
            System.out.println("x is " + x);
            if (x == 30) {
                break;
            }
        }


        // break label
        label:
        for (int i = 0; i < 3; i++) {
            for (int x : numbers) {
                System.out.println("x is " + x);
                if (x == 30) {
                    break label;
                }
            }
            System.out.println("i = " + i);
        }


    }

}
