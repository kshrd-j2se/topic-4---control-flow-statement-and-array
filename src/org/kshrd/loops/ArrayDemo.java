package org.kshrd.loops;

public class ArrayDemo {

    public static void main(String[] args) {

        //TODO: --- single dimensional array ---
        int []arr; // declaration
        arr = new int[5]; // instantiation
        arr[0] = 10; // initialization
        arr[1] = 20;
        arr[2] = 30;
        arr[3] = 40;
        arr[4] = 50;

        //  ** We also can create array like this **
        // int []arr = {10, 20, 30, 40, 50};

        for (int i=0; i<arr.length; i++) {
            System.out.println( arr[i] );
        }


        //TODO: --- multiple dimensional array ---
        String [][] numbers;  //
        numbers = new String[2][4]; // instantiation

        numbers[0][0] = "00";
        numbers[0][1] = "01";

        numbers[1][0] = "10";
        numbers[1][1] = "11";
        numbers[1][2] = "12";
        numbers[1][3] = "13";


        // or can create like this
        String [][] number1 = {
                {"00","01", "02"},
                {"10","11", "12", "13"}
        };

        for (int r=0; r<numbers.length; r++) {
            for (int c=0; c<numbers[r].length; c++) {
                System.out.print(numbers[r][c]);
                System.out.print("\t");
            }
            System.out.println();
        }





    }


}
