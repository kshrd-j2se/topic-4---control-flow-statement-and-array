package org.kshrd.loops;

public class WhileLoop {
    public static void main(String[] args) {

        int x = 30;
        while (x < 20) {
            System.out.println("X is " + x);
            x++;
        }

        System.out.println("x outside " + x);

    }
}
