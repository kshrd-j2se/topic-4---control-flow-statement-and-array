package org.kshrd.loops;

public class DoWhileLoop {

    public static void main(String[] args) {

        int x = 30;
        do {
            System.out.println("x = " + x);
            x++;
        } while (x < 20);


    }
}
