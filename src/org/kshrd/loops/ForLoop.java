package org.kshrd.loops;

public class ForLoop {

    public static void main(String[] args) {

        // for normal
        int i = 1;
        for (i=1; i<5; i--) {
            System.out.println(i);
        }
        System.out.println("i out side : " + i);

        // for each
        int arr[] = {24,56,7,8,3};
        // print arr with for each
        for (int a : arr) {
            System.out.println(a);
        }

        // print arr with normal loop
        for (int index=0; index<arr.length; index++){
            System.out.println(arr[index]);
        }

        System.out.println(arr);





    }

}
