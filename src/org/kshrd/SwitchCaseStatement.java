package org.kshrd;

import java.text.DecimalFormat;
import java.util.Scanner;

public class SwitchCaseStatement {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter grade:");
        char grade = scanner.next().charAt(0);

        switch (grade) {
            case 'A':
                System.out.println("Excellent!");
                break;
            case 'B':
                System.out.println("Well done");
            case 'C':
                System.out.println("Very good");
                break;
            case 'D':
                System.out.println("You pass");
                break;
            default:
                System.out.println("fails");
        }

        // format only 2 digit after .
        /*
        double d = 5.84848;
        System.out.println(new DecimalFormat("##.##").format(d));
        */
    }

}
