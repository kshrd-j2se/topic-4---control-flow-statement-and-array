package org.kshrd;

import java.util.Scanner;

public class IfStatement {

    public static void main(String[] args) {

        // if...else Statement
        int x = 40;
        if (x > 30) {
            System.out.println("x == 30");
        } else {
            System.out.println("x != 30");
        }


        // if...else if statement
        if (x == 10) {
            System.out.println("x is 10");
        } else if (x == 20) {
            System.out.println("x is 20");
        } else if (x == 30) {
            System.out.println("x is 30");
        } else {
            System.out.println("this is else statement");
        }


        // Read from keyboard
        System.out.println("Please Input your age : ");
        Scanner sc = new Scanner(System.in);
        int age = sc.nextInt();

        if (age < 20)
            System.out.println("I am a teenager");
        else if (age > 20)
            System.out.println("I am not a teenager");

    }
}
